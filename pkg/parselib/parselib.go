package parselib

import (
	"awesomeProject3/domain"
	_ "awesomeProject3/domain"
	"log"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func createBashQuotes() *domain.BashQuotes {
	return &domain.BashQuotes{ListQuotes: make([]string, 0, 1000)}
}

func GetBashQuotes() *domain.BashQuotes {
	// Request the HTML page.
	bQuotes := createBashQuotes()

	res, err := http.Get("https://bash.im/best")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	nodes := doc.Find(".quote__body").Nodes

	for _, node := range nodes {
		quote := strings.Replace(node.FirstChild.Data, "\n", "", -1)
		quote = strings.Replace(quote, " ", "", -1)

		currentSibling := node.FirstChild.NextSibling
		for currentSibling != nil {
			dataSibling := currentSibling.Data
			if dataSibling == "br" {
				quote += "\n"
			} else {
				quote += dataSibling
			}
			currentSibling = currentSibling.NextSibling
		}

		bQuotes.ListQuotes = append(bQuotes.ListQuotes, quote)
	}

	return bQuotes
}
