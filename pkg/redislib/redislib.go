package redislib

import (
	"github.com/go-redis/redis"
)

func SendJsonDataToRedis(jsonData []byte) {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:32768",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	err := client.Set("quotes", jsonData, 0).Err()
	if err != nil {
		panic(err)
	}
}

func RecieveJsonDataFromRedis() string {

	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:32768",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	val, err := client.Get("quotes").Result()
	if err != nil {
		panic(err)
	}

	return val
}
