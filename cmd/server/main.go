package main

import (
	"awesomeProject3/pkg/parselib"
	"awesomeProject3/pkg/redislib"
	_ "awesomeProject3/pkg/redislib"
	"encoding/json"
	"fmt"
	"log"
)

func main() {
	bQuoutes := parselib.GetBashQuotes()
	jsonString, err := json.Marshal(bQuoutes)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("quotes recive from server")

	redislib.SendJsonDataToRedis(jsonString)

	fmt.Println("quotes send to redis")

}
