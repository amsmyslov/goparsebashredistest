package main

import (
	"awesomeProject3/domain"
	"awesomeProject3/pkg/redislib"
	"encoding/json"
	"fmt"
	"log"
)

func main() {
	jsonResponse := redislib.RecieveJsonDataFromRedis()
	var bQuotes domain.BashQuotes
	err := json.Unmarshal([]byte(jsonResponse), &bQuotes)

	if err != nil {
		log.Fatal(err)
	}
	for _, quote := range bQuotes.ListQuotes {
		fmt.Print(quote)
		fmt.Println("\n------\n\n")
	}
}
